# Normal Stock Price Calculator

## Functional Programming B: Group 14

- Alsabila Shakina Prasetyo - 1606917613
- Ignatius Rahardi P. - 1606875951
- Justin - 1606878871
- Kevin Prakasa Anggono - 1606917696
- Swastinika Naima Moertadho - 1606821854

**Halaman URL**: https://hitunghargawajarsaham.herokuapp.com/

## Application Description

This application calculates the normal stock price for a certain company or asset. The normal stock price is based on the current stock price and certain data released in financial reports by said company.

**Wireframe**: https://www.figma.com/file/BZQWdhsAtgQbgbgZxHICX41i/Pemfung?node-id=0%3A1

**POST method endpoint:**
- DCF : https://og7b5no2jg.execute-api.ap-southeast-1.amazonaws.com/default/dcf-function
- Graham : https://5y4ipekwh1.execute-api.ap-southeast-1.amazonaws.com/default/graham-function
- PBV : https://nfl85f09kd.execute-api.ap-southeast-1.amazonaws.com/default/pbv-pemfung 
- PER: https://fgkcg3yzy0.execute-api.ap-southeast-1.amazonaws.com/default/per-function

## Methods of Valuating Stock

### Benjamin Graham Formula

This method was introduced by the father of value investing, Benjamin Graham, in his book The Intelligent Investor.

The original formula for Benjamin Graham method is this:

```
Value = (EPS x (8.5 + 2g) x 4.4) / Y

EPS: Earnings per share, gained by dividing net profit by amount of stock in the market.
g: Growth rate of company, capped at 15%.
Y: AAA corporate bond yield
```

Rivan Kurniawan (http://rivankurniawan.com/), an Indonesian value investor wrote about a version of Benjamin Graham formula, adjusted to the Indonesian market. The formula is as follows:

```
Value = (EPS x (7 + 1g) x 7.8) / Y

Y: AAA corporate bond yield for Indonesia is constant at 11.4%.
```

### PBV Analysis

- This method calculates from 5 years data of stock's PBV (Price Book Value). PBV (Price Book Value) is a ratio between stock's price and book value per share and Book value/share is the value of an asset which calculated from equity's value / total stock shares.

```
BVPS (Book Value per Share) = Equity's value / total shares (total lembar saham)
PBV = Stock's price / BVPS

Normal Stock Price = (pbv1+pbv2+pbv3+pbv4+pbv5)/5) * bvps
```

PBV Category:

- PBV < 1 -> The stock is undervalued
- PBV = 1 -> The stock is on its normal value
- PBV > 1 -> The stock is overvalued

### PER Analysis

- This method is pretty similar with PBV Analysis, but in this case we use PER (Price Earning Ratio) which PER is a ratio between stock's price and EPS (Earnings Per Share) TTM

```
PER = Stock's price / EPS ttm
Normal Stock Price = (per1+per2+per3+per4+per5)/5) * eps
```

- We take all PER from five years back, and then take the mean from it. Use that mean to calculate the normal price of its stock.

### DCF Method

- This method explanations is pretty long, so it better you go to this reference directly [click me](https://www.tnp-capital.com/2015/02/cara-menghitung-harga-wajar-saham.html)

# How to Run Scripts

Frontend folder for functional making a computing finance tool

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
