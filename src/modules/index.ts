import { combineReducers, Reducer } from 'redux';
import { connectRouter } from 'connected-react-router';

export default (history: any): Reducer => combineReducers({
  router: connectRouter(history),
});
