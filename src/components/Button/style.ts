import styled from 'styled-components';

const ButtonContainer = styled.div`
  .btn-component-wrap {
    background: ${(props): string => props.theme.colors.gray};
    padding: 0.125rem 4rem;
    cursor: pointer;
  }

  .btn-component-wrap:hover {
  }
`;

export default ButtonContainer;
