import React from 'react';

import ButtonContainer from './style';

type ButtonProps = {
  buttonLabel: string;
  onClick?: (e: React.MouseEvent) => void;
}

const Button: React.FC<{
  buttonLabel: string;
  onClick?: (e: React.MouseEvent) => void;
}> = ({ buttonLabel, onClick }: ButtonProps) => (
  <ButtonContainer>
    <button
      type="button"
      className="btn-component-wrap"
      onClick={onClick}
    >
      <p>{buttonLabel}</p>
    </button>
  </ButtonContainer>
);

export default Button;
