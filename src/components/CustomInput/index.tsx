import React from "react";

import CustomInputContainer from "./style";

type CustomInputProps = {
  InputLabel: string;
  InputType: string;
  InputName?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

const CustomInput: React.FC<{
  InputLabel: string;
  InputType: string;
  InputName?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}> = ({ InputLabel, onChange, InputType, InputName }: CustomInputProps) => (
  <CustomInputContainer>
    <p className="custom-input-label">{InputLabel}</p>
    <input
      className="custom-input"
      type={InputType}
      name={InputName}
      onChange={onChange}
    />
  </CustomInputContainer>
);

export default CustomInput;
