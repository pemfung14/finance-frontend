import { DefaultTheme } from 'styled-components';

// Put colors, and other style definition here,
// after modifying the interface in /styled.d.ts
const theme: DefaultTheme = {
  borderRadius: '5px',

  colors: {
    main: 'black',
    secondary: 'white',
    gray: '#C4C4C4',
  },
};

export default theme;
