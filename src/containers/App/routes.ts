import HomePage from 'containers/HomePage';
import NotFoundPage from 'containers/NotFoundPage';
import { FunctionComponent } from 'react';

const routes: Record<string, FunctionComponent | boolean | string>[] = [
  {
    component: HomePage,
    exact: true,
    path: '/',
  },
  {
    component: NotFoundPage,
  },
];

export default routes;
