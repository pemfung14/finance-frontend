import React from 'react';
import { ThemeProvider } from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import theme from './theme';
import routes from './routes';


import AppContainer from './style';

const App: React.FC = () => {
  const pages = routes.map(
    (route: Record<string, any>) => (
      <Route
        key={`${route.component};${route.path}`}
        component={route.component}
        exact={route.exact}
        path={route.path}
      />
    ),

  );

  return (
    <ThemeProvider theme={theme}>
      <AppContainer>
        <Switch>{pages}</Switch>
      </AppContainer>
    </ThemeProvider>
  );
};

export default App;
