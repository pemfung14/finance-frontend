import React, { useState } from 'react';
import Button from 'components/Button';
import CustomInput from 'components/CustomInput';

import HomePageContainer from './style';

const GrahamMethod: React.FC = () => {
  // const [eps, setEps] = useState(0.0);
  // const [growth, setGrowth] = useState(0.0);
  const endpoint = 'https://5y4ipekwh1.execute-api.ap-southeast-1.amazonaws.com/default/graham-function';
  const [value, setValue] = useState(0.0);
  const [grahamInput, setGraham] = useState({
    eps: 0,
    growth : 0
  })

  function handleInputChange(e : React.ChangeEvent<HTMLInputElement>) {
    const {name , value} = e.target;
    setGraham(
      {...grahamInput,
      [name] : parseFloat(value)
      });
      console.log(grahamInput)
  }

  async function calculate () {
    try {
      const response = await fetch(endpoint, {
        method: 'POST',
        body: JSON.stringify(grahamInput)
      });
      const result = await response.json();
      setValue(parseFloat(JSON.stringify(result)))
      console.log('Success:', JSON.stringify(result));
    } catch (error) {
      console.log('SError:', JSON.stringify(error));
    }
  }

  return (<HomePageContainer>
    <section id="landing-hero">
      <div className="hero-container">
        <div className="hero-word-wrap">
          <div className="word-subtitle">
            <p className="subtitle" style={{ textDecorationLine: 'underline' }}>
              Benjamin Graham Method
            </p>
          </div>
          <div className="variables" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly' }}>
            <div className="inputs" style={{ textAlign: 'justify' }}>
              <div style={{ fontSize: 18 }}>
                <CustomInput
                  InputLabel="Earnings per Share (EPS)"
                  InputType="number"
                  InputName="eps"
                  onChange={
                    handleInputChange
                  }
                />
              </div>  
              <p style={{ fontSize: 18 }}>Company's Growth Rate</p>
              <input name ="growth" type="number" style={{ height: 30, width: 120 }}
              onChange={
                handleInputChange
              } />
              <p style={{ display: "inline", fontSize: 24 }}> %</p>
            </div>
            <div>
              <p style={{ fontWeight: 'bold', fontSize: 20, paddingTop: 20 }}>Normal Stock Value:</p>
            <p style={{ fontSize: 20 }}>Rp. {value}</p>
              <div className="btn-wrap" style={{ margin: '3rem 0 0 2rem' }}>
                <Button
                  buttonLabel="CALCULATE"
                  onClick={ calculate }
                />
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
    <hr className="divider" />
  </HomePageContainer>
  )
};

export default GrahamMethod;
