import React, { useState, ChangeEvent } from 'react';
import Button from 'components/Button';
import CustomInput from 'components/CustomInput';

import HomePageContainer from './style';

const PERMethod: React.FC = () => {
  const endpoint = 'https://fgkcg3yzy0.execute-api.ap-southeast-1.amazonaws.com/default/per-function';
  const [nsv, setNsv] = useState(0);
  const [perInput, setPer] = useState({
    per1: 0,
    per2: 0,
    per3: 0,
    per4: 0,
    per5: 0,
    epsttm: 0
  });

  function handleInputChange(e : React.ChangeEvent<HTMLInputElement>) {
    const {name , value} = e.target;
    setPer(
      {...perInput,
      [name] : parseFloat(value)
      });
      console.log(perInput)
  }

  async function calculate () {
    // try {
      // const response = await fetch(endpoint, {
      //   method: 'POST',
      //   body: JSON.stringify(perInput)
    //   });
    //   const result = await response.json();
    //   setNsv(parseFloat(JSON.stringify(result)))
    //   console.log('Success:', JSON.stringify(result));
    // } catch (error) {
    //   console.log('SError:', JSON.stringify(error));
    // }
    fetch(endpoint, {
      method: 'POST',
      body: JSON.stringify(perInput)
    })
      .then(res=>res.json())
      .then(res => setNsv(parseFloat(JSON.stringify(res))))
      .catch(err => alert(err))
  }
  return (
  <HomePageContainer>
    <section id="landing-hero">
      <div className="hero-container">
        <div className="hero-word-wrap">
          <div className="word-subtitle">
            <p className="subtitle" style={{ textDecorationLine: 'underline' }}>
              PER Analysis
            </p>
          </div>
          <div className="variables" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly' }}>
            <div className="per" style={{ textAlign: 'justify' }}>
              <p style={{ fontSize: 18 }}>*PER: Price Earning Ratio</p>
              <p style={{ fontSize: 18 }}>EPS: Earnings Per Sales</p>
              <div style={{ fontSize: 18 }}>
                <CustomInput
                  InputLabel="PER Year 1*"
                  InputType="number"
                  InputName="per1"
                  onChange={
                    handleInputChange
                  }
                />
              </div>
              {/* <p style={{ fontSize: 18 }}>PER Year 1*</p>
              <input type="text" style={{ height: 30 }} /> */}
               <div style={{ fontSize: 18 }}>
                <CustomInput
                  InputLabel="PER Year 2*"
                  InputType="number"
                  InputName="per2"
                  onChange={
                    handleInputChange
                  }
                />
              </div>
              <div style={{ fontSize: 18 }}>
                <CustomInput
                  InputLabel="PER Year 3*"
                  InputType="number"
                  InputName="per3"
                  onChange={
                    handleInputChange
                  }
                />
              </div>
              <div style={{ fontSize: 18 }}>
                <CustomInput
                  InputLabel="PER Year 4*"
                  InputType="number"
                  InputName="per4"
                  onChange={
                    handleInputChange
                  }
                />
              </div>
              <div style={{ fontSize: 18 }}>
                <CustomInput
                  InputLabel="PER Year 5*"
                  InputType="number"
                  InputName="per5"
                  onChange={
                    handleInputChange
                  }
                />
              </div>
              <div style={{ fontSize: 18 }}>
                <CustomInput
                  InputLabel="EPS TTM*"
                  InputType="number"
                  InputName="epsttm"
                  onChange={
                    handleInputChange
                  }
                />
              </div>
            </div>
            <div>
              <p style={{ fontWeight: 'bold', fontSize: 20, paddingTop: 20 }}>Normal Stock Value:</p>
              <p style={{ fontSize: 20 }}>Rp. {nsv}</p>
              <div className="btn-wrap" style={{ margin: '3rem 0 0 2rem' }}>
                <Button
                  buttonLabel="CALCULATE"
                  onClick={calculate}
                />
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
    <hr className="divider" />
  </HomePageContainer>
  )
};

export default PERMethod;
