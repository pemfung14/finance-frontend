import styled from "styled-components";

const HomePageContainer = styled.div`
  #landing-hero {
    position: relative;
    min-height: 100vh;
    .hero-container {
      display: flex;
      padding: 4rem;
      text-align: center;
      flex-direction: column;

      .hero-word-wrap {
        width: 100%;
        text-align: center;

        .word-title {
          .title {
            font-size: 76px;
            color: ${(props): string => props.theme.colors.main};
          }
        }

        .word-subtitle {
          width: 50%;
          margin: auto;
          .subtitle {
            font-size: 24px;
          }
        }
      }

      .hero-button-wrap {
        margin: 4rem 0;

        img {
          width: 50px;
        }
      }
    }

    .divider {
      position: absolute;
      bottom: 0;
    }
  }
`;

export default HomePageContainer;
