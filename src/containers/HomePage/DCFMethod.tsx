import React, { useState } from 'react';
import HomePageContainer from './style';
import Button from 'components/Button';

const DCFMethod: React.FC = () => {
  const [eps1, setEps1] = useState(0);
  const [eps2, setEps2] = useState(0);
  const [eps3, setEps3] = useState(0);
  const [eps4, setEps4] = useState(0);
  const [eps5, setEps5] = useState(0);
  const [per1, setPer1] = useState(0);
  const [per2, setPer2] = useState(0);
  const [per3, setPer3] = useState(0);
  const [per4, setPer4] = useState(0);
  const [per5, setPer5] = useState(0);
  const [nsv, setNsv] = useState(0);

  function onChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const { name } = event.target;
    const value: number = +event.target.value;

    switch (name) {
      case 'eps1': setEps1(value); break;
      case 'eps2': setEps2(value); break;
      case 'eps3': setEps3(value); break;
      case 'eps4': setEps4(value); break;
      case 'eps5': setEps5(value); break;
      case 'per1': setPer1(value); break;
      case 'per2': setPer2(value); break;
      case 'per3': setPer3(value); break;
      case 'per4': setPer4(value); break;
      case 'per5': setPer5(value); break;
      default: break;
    }
  }

  return (
    <HomePageContainer>
      <section id="landing-hero">
        <div className="hero-container">
          <div className="hero-word-wrap">
            <div className="word-subtitle">
              <p className="subtitle" style={{ textDecorationLine: 'underline' }}>
                DCF Method
              </p>
            </div>
            <div className="variables" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly' }}>
              <div className="eps" style={{ textAlign: 'justify' }}>
                <p style={{ fontSize: 18 }}>*EPS: Earnings Per Share</p>
                <p style={{ fontSize: 18 }}>EPS Year 1*</p>
                <input type="number" style={{ height: 30 }} onChange={onChange} value={eps1} name="eps1" />
                <p style={{ fontSize: 18 }}>EPS Year 2*</p>
                <input type="number" style={{ height: 30 }} onChange={onChange} value={eps2} name="eps2" />
                <p style={{ fontSize: 18 }}>EPS Year 3*</p>
                <input type="number" style={{ height: 30 }} onChange={onChange} value={eps3} name="eps3" />
                <p style={{ fontSize: 18 }}>EPS Year 4*</p>
                <input type="number" style={{ height: 30 }} onChange={onChange} value={eps4} name="eps4" />
                <p style={{ fontSize: 18 }}>EPS Year 5*</p>
                <input type="number" style={{ height: 30 }} onChange={onChange} value={eps5} name="eps5" />

                <p style={{ fontWeight: 'bold', fontSize: 20, paddingTop: 20 }}>Normal Stock Value:</p>
                <p style={{ fontSize: 20 }}>Rp. {nsv}</p>
              </div>
              <div className="per" style={{ textAlign: 'justify' }}>
                <p style={{ fontSize: 18 }}>PER: Price Earning Ratio</p>
                <p style={{ fontSize: 18 }}>PER Year 1*</p>
                <input type="number" style={{ height: 30 }} onChange={onChange} value={per1} name="per1" />
                <p style={{ fontSize: 18 }}>PER Year 2*</p>
                <input type="number" style={{ height: 30 }} onChange={onChange} value={per2} name="per2" />
                <p style={{ fontSize: 18 }}>PER Year 3*</p>
                <input type="number" style={{ height: 30 }} onChange={onChange} value={per3} name="per3" />
                <p style={{ fontSize: 18 }}>PER Year 4*</p>
                <input type="number" style={{ height: 30 }} onChange={onChange} value={per4} name="per4" />
                <p style={{ fontSize: 18 }}>PER Year 5*</p>
                <input type="number" style={{ height: 30 }} onChange={onChange} value={per5} name="per5" />
                <div className="btn-wrap" style={{ margin: '3rem 0 0 0rem' }}>
                  <Button
                    buttonLabel="CALCULATE"
                    onClick={ async () => {
                      try {
                        const response = await fetch('https://og7b5no2jg.execute-api.ap-southeast-1.amazonaws.com/default/dcf-function', {
                          method: 'POST',
                          body: JSON.stringify({
                              "eps1": eps1,
                              "eps2": eps2,
                              "eps3": eps3,
                              "eps4": eps4,
                              "eps5": eps5,
                              "per1": per1,
                              "per2": per2,
                              "per3": per3,
                              "per4": per4,
                              "per5": per5
                          })
                        });
                        const result = await response.json();
                        setNsv(result);
                        console.log('Success:', JSON.stringify(result));
                      } catch (error) {
                        console.error('Error:', error);
                      }
                    } 
                  }
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <hr className="divider" />
    </HomePageContainer>
  );
};

export default DCFMethod;
