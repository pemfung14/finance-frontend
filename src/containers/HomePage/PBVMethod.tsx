import React, { useState, useEffect, ChangeEvent } from "react";
import Button from "components/Button";
import CustomInput from "components/CustomInput";
import HomePageContainer from "./style";

const endpointPBV: string =
  "https://nfl85f09kd.execute-api.ap-southeast-1.amazonaws.com/default/pbv-pemfung";

interface PBV1 {
  InputLabel: string;
  InputType: string;
  InputName: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const PBVMethod: React.FC = () => {
  const [pbvInput, setPbvInput] = useState({
    pbv1: 0,
    pbv2: 0,
    pbv3: 0,
    pbv4: 0,
    pbv5: 0,
    bvps: 0
  });

  const [normalPrice, setNormalPrice] = useState(0);

  const postPBVInput = async (e: React.MouseEvent) => {
    try {
      const data = await fetch(endpointPBV, {
        method: "POST",
        body: JSON.stringify(pbvInput)
      });
      const response = await data.json();
      setNormalPrice(parseFloat(JSON.stringify(response)));
    } catch (error) {
      console.log(error);
    }
  };

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e);
    const { name, value } = e.target;
    setPbvInput({
      ...pbvInput,
      [name]: parseFloat(value)
    });
  };

  const createSingleCustomInput = (item: PBV1) => <CustomInput {...item} />;

  const createMultipleCustomInput = () => {
    const items = [
      {
        InputLabel: "PBV Year 1*",
        InputType: "number",
        InputName: "pbv1",
        onChange: handleInputChange
      },
      {
        InputLabel: "PBV Year 2*",
        InputType: "number",
        InputName: "pbv2",
        onChange: handleInputChange
      },
      {
        InputLabel: "PBV Year 3*",
        InputType: "number",
        InputName: "pbv3",
        onChange: handleInputChange
      },
      {
        InputLabel: "PBV Year 4*",
        InputType: "number",
        InputName: "pbv4",
        onChange: handleInputChange
      },
      {
        InputLabel: "PBV Year 5*",
        InputType: "number",
        InputName: "pbv5",
        onChange: handleInputChange
      },
      {
        InputLabel: "BVPS*",
        InputType: "number",
        InputName: "bvps",
        onChange: handleInputChange
      }
    ];
    return items.map(createSingleCustomInput);
  };

  return (
    <HomePageContainer>
      <section id="landing-hero">
        <div className="hero-container">
          <div className="hero-word-wrap">
            <div className="word-subtitle">
              <p
                className="subtitle"
                style={{ textDecorationLine: "underline" }}
              >
                PBV Analysis
              </p>
            </div>
            <div
              className="variables"
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-evenly"
              }}
            >
              <div className="PBV" style={{ textAlign: "justify" }}>
                <p style={{ fontSize: 18 }}>*PBV: Price Book Value</p>
                <p style={{ fontSize: 18 }}>BVPS: Book Value per Share</p>
                <div style={{ fontSize: 18 }}>
                  {createMultipleCustomInput()}
                </div>
              </div>
              <div>
                <p style={{ fontWeight: "bold", fontSize: 20, paddingTop: 20 }}>
                  Normal Stock Value:
                </p>
                <p style={{ fontSize: 20 }}>Rp. {normalPrice}</p>
                <div className="btn-wrap" style={{ margin: "3rem 0 0 2rem" }}>
                  <Button buttonLabel="CALCULATE" onClick={postPBVInput} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <hr className="divider" />
    </HomePageContainer>
  );
};

export default PBVMethod;
