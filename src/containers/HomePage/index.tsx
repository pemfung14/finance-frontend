import React from "react";
import Arrow from "assets/Arrow.svg";
import HomePageContainer from "./style";
import GrahamMethod from "./GrahamMethod";
import DCFMethod from "./DCFMethod";
import PERMethod from "./PERMethod";
import PBVMethod from "./PBVMethod";

const HomePage: React.FC = () => (
  <HomePageContainer>
    <section id="landing-hero">
      <div className="hero-container">
        <div className="hero-word-wrap">
          <div className="word-title">
            <h1 className="title">Welcome to Our App!</h1>
          </div>

          <div className="word-subtitle">
            <p className="subtitle">
              this application will help you calculate the normal price of any
              given stock. No more worrying about the worthiness of the price
              you&apos;re paying
            </p>
          </div>
        </div>
        <div className="hero-button-wrap">
          <img src={Arrow} alt="Arrow.svg" />
        </div>
      </div>
    </section>
    <hr className="divider" />
    <GrahamMethod />
    <PBVMethod />
    <PERMethod />
    <DCFMethod />
  </HomePageContainer>
);

export default HomePage;
